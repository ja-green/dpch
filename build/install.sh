#!/usr/bin/env bash

topl="$(git rev-parse --show-toplevel || echo "${PWD}/..")"
blds="${topl}/build"
cmds="${topl}/bin"
libs="${topl}/lib"
home="${HOME}/.dpch"
user="${SUDO_USER}"

version="v$(git describe --tags --always --dirty || echo 'unversioned build')"

die() {
  printf "fatal: %s\n" "${1}"
  exit 1
}

check_priv() {
  [ "${EUID}" -ne 0 ] \
    && die "install must be run as root"

  [ "${BASH_VERSINFO}" -lt 4 ] \
    && die "bash version 4.0 or greater is required to install 'dpch'"
}

ensure_dirs() {
  mkdir -p "/usr/lib/dpch"
  mkdir -p "${home}/vuln"
  mkdir -p "${home}/var"

  chown -R "${user}":"${user}" "${home}"
}

preprocess() {
  cd "${libs}"

  type cpp &>/dev/null \
    || die "program 'cpp' not found"

  mkdir -p "${blds}/cpp"

  for cmd in "${cmds}"/*; do
    touch "${blds}/cpp/${cmd##*/}"
    cpp -traditional-cpp -P -E "${cmd}" -DVERSION="${version}" | sed '1,13d' > "${blds}/cpp/${cmd##*/}"
  done

  cpp -traditional-cpp -P -E "${topl}/ngt" | sed '1,13d' > "${blds}/cpp/dpch"
  printf "%s\n\n%s" "#!/usr/bin/env bash" "$(cat "${blds}"/cpp/dpch)" > "${blds}/cpp/dpch"
}

move_builtins() {
  for cmd in "${blds}/cpp"/*; do
    cmd_name="${cmd%.*}"
    cmd_name="${cmd_name##*/}"

    cp "${cmd}" "/usr/lib/dpch/dpch-${cmd_name}"
  done
}

move_main() {
  cp "${blds}/cpp/dpch" "/usr/local/bin"
}

main() {
  check_priv
  ensure_dirs
  preprocess
  move_builtins
  move_main
}

main
