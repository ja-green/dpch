msg__usage() {
: << EOD
  msg__usage
  
  prints a specified usage string to stdout (fd1)
  and terminates execution with a successful exit status
  
  exits 0
EOD
  
  printf "%b\n" "${1}"
  
  exit 0 
}

msg__debug() {
: << EOD
  msg__debug
  
  prints a specified debug string to stderr (fd2)
  prefixed with 'debug:' if debugging is enabled
  
  returns 0 
EOD    

  [ ! -z "${debug}" ] \
    && printf "debug: %b\n" "${1}" >&2
}

msg__warn() {
: << EOD
  msg__warn
  
  prints a specified warning string to stderr (fd2)
  prefixed with 'warn:'
  
  returns 0
EOD

  printf "warn: %b\n" "${1}" >&2
}

msg__error() {
: << EOD
  msg__error
  
  prints a specified error string to stderr (fd2)
  prefixed with 'error:' and terminates execution
  with a failure exit status

  exits 1
EOD

  printf "error: %b\n" "${1}" >&2
  
  exit 1
}

msg__die() {
: << EOD
  msg__die
  
  prints a specified critial error string to stderr (fd2)
  prefixed with 'fatal:' and terminates execution
  with a failure exit status
  
  exits 1
EOD

  printf "fatal: %b\n" "${1}" >&2
  
  exit 1
}
