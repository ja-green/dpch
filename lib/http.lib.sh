http__get() {
: << EOD
  http__get
  
  sends a GET request to the specified url
  
  returns the returned content of the response
EOD

  declare url="${1}"
  
  curl --silent --request GET \
    "http://${url}"
}

http__post() {
: << EOD
  http__post
  
  sends a POST request to the specified url with the
  specified post parameters
  
  returns the returned content of the response
EOD

  declare url="${1}" params=(${2//:/ })
  
  for param in "${params[@]}"; do
    post_flags+="-d ${param} "
  done
  
  curl --silent --request POST \
    --header "X-Forwarded-For: ${ip}" \
    "http://${url} ${post_flags}"
}

http__get_status() {
: << EOD
  http__get_status
  
  sends a GET request to the specified url discarding
  all returned content except the http status code

  returns the http status code of the response
EOD

  declare url="${1}" http_status
  
  http_status="$(curl -o /dev/null -sw "%{http_code}" "http://${url}")"
  
  printf "%s" "${http_status}"
}
