runtime__init_conf() {
: << EOD
  runtime__init_conf
  
  attempts to locate and execute the builtin command
  'parse-config' or evaluate the cached config values
  in order to setup the runtime environment
  returns 0 if environment was set up correctly
  returns 1 if there was an issue with environment setup
EOD

  [ -f "/usr/lib/dpch/dpch-parse-config" ] \
    && source "/usr/lib/dpch/dpch-parse-config"
      
  [ -f "${HOME}/.dpch/var/rc-cache" ] \
    && eval "$(cat "${HOME}/.dpch/var/rc-cache")"
    
  [ -f "${path[exec]}/dpch-parse-config" ] \
    && source "${path[exec]}/dpch-parse-config"
      
  declare -Ap | grep -q keepalive \
    && return 0 \
    || return 1
}

runtime__init_path() {
: << EOD
  runtime__init_conf
  
  attempts to locate evalute the paths
  where nugget command binaries and runtime
  configuration files are located
  
  returns 0 if the paths were set up correctly
  returns 1 if there was an issue with path setup
EOD

  [ -z "${DPCH_HOME}" ] && [ -z "${path[home]}"  ] \
    && return 1 \
    || dpch_home="${DPCH_HOME:-${path[home]}}"

  [ -z "${exec_path}" ] && [ -z "${path[exec]}"  ] \
    && exec_path="/usr/lib/dpch/" \
    || exec_path="${exec_path:-${path[exec]}}" 

  [ -z "${conf_path}" ] && [ -z "${path[dpchrc]}" ] \
    && conf_path="${HOME}" \
    || conf_path="${conf_path:-${path[dpchrc]}}"

  declare -g vuln_d="${dpch_home}/vuln"
  declare -g ncve_d="${dpch_home}/nvdcve"
}

runtime__levenshtein() {
: << EOD
  runtime__levenshtein
  
  calculates the levenshtein distance between
  the input command and builtin commands and
  outputs all commands with a distance
  greater than 0.25
  
  returns 0
EOD

  declare advice_msg lev_dist lev_cnt lev_res

  for builtin in $(find "${exec_path}" -maxdepth 1 -type f) $(compgen -ac | grep 'dpch-' | sed -e 's/^dpch-//') ${!alias[@]}; do
    [ "${builtin}" = "keepalive" ] && continue

    lev_dist=$(awk '
      function min(x, y) {
        return x < y ? x : y
      }
      function lev(s,t) {
        m = length(s)
        n = length(t)

        for(i=0;i<=m;i++) d[i,0] = i
        for(j=0;j<=n;j++) d[0,j] = j

        for(i=1;i<=m;i++) {
          for(j=1;j<=n;j++) {
            c = substr(s,i,1) != substr(t,j,1)
            d[i,j] = min(d[i-1,j]+1,min(d[i,j-1]+1,d[i-1,j-1]+c))
          }
        }

        return d[m,n]
      }

      BEGIN {print 1 / (1 +lev(ARGV[1], ARGV[2])); exit}' "${cmd}" "${builtin##*dpch-}"
    )

    (($(echo "${lev_dist} > 0.25" | bc -l))) \
      && lev_res+="  ${builtin##*dpch-}\n" \
      && lev_cnt+=1
  done

  [ ${lev_cnt:-0} -gt 1 ] \
    && advice_msg="the most similar commands are" \
    || advice_msg="the most similar command is"

  [ -z "${lev_res}" ] \
    && printf "\n" \
    || printf "\n%s\n%s\n" "${advice_msg}" "${lev_res}"
}

