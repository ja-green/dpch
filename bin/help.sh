#include "../lib/msg.lib.sh"

main_usage="\
usage: dpch [-v | --version] [-h | --help] [-C <path>]
        [-c <name>=<value>] [--exec-path] [--dpchrc-path]
        <command> [<subcommand>] [<arguments>]
        
dpch commands for various situations:

scan              scan a repository for vulnerable dependencies
help              view help information and documentation
version           view version and licensing information

custom commands can be defined by creating an executable
file named 'dpch-<command-name>' and placing it in your \$PATH

see 'dpch help -a' for a list of available commands.
see 'dpch help <command>' or 'dpch help <concept>' to
read about a specific subcommand or concept" 

help_usage="\
usage:
  dpch help [options]
  dpch help [command]
  dpch help [concept]

options:
  -a, --all      print all available commands
  -m, --man      show man page
  -l, --license  show license information
  -h, --help     show this help screen
  -Z, --debug    show debug information
  
see 'ngt help help' for more information" 

commands_list="\
builtin commands available from '/usr/lib/dpch':
  scan      scan a repository
  help      show help information
  version   show version information"

list_commands() {
  custom_extrn="$(compgen -ac | grep 'dpch-' | sed -e 's/^dpch-//')"
  custom_alias="${!alias[@]}" 
  
  printf "%s\n" "${commands_list}"

  if [ ! -z "${custom_extrn}" ]; then
    msg__debug "found custom external commands"
    
    printf "\ncustom commands available from elsewhere in your \$PATH:\n"
    
    for command in ${custom_extrn}; do
      printf "  %s\n" "${command}"
    done
  fi

  if [ ! -z "${custom_alias}" ]; then
    msg__debug "found alias commands"
    
    printf "\ncustom aliases defined in '${conf_path}/.dpchrc':\n"
    
    for alias in ${custom_alias}; do
      [ "${alias}" = "keepalive" ] \
        && continue
      
      printf "  %s\n" "${alias}"
    done
  fi

  printf "\nsee 'dpch help' for help with a specific command or concept\n"
}

cmd_help() {
  if [ ! -z "${all}" ]; then
    list_commands

  elif [ ! -z "${man}" ]; then
    man ngt 2>/dev/null \
      || msg__error "no help text for 'dpch'"

  elif [ ! -z "${license}" ]; then
    printf "${license_info}\n"
  
  elif [ -z "${sub_cmd}" ]; then
    msg__usage "${main_usage}"

  else
    man "dpch-${sub_cmd}" 2>/dev/null      \
      || man "dpch-${sub_cmd}" 2>/dev/null \
      || msg__error "no help text for '${sub_cmd}'"
  fi
}

main() {
  while [ -n "${1}" ]; do
    case "${1}" in
    --) shift; break ;;
    -*) case "${1}" in
    -a|--all)     all=1 ;;
    -m|--man)     man=1 ;;
    -l|--license) license=1 ;;
    -Z|--debug)   debug=1 ;;
    -h|--help)    msg__usage "${help_usage}" ;;
    -*)           msg__die "unknown option '${1}'" ;;
    esac ;;

    *) [ -z "${sub_cmd}" ] && sub_cmd="${1}" ;;

    esac
    shift

  done

  msg__debug "got flags '${argv%% }'"
  
  cmd_help
}

main ${@}

exit 0
