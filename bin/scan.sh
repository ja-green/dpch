#include "../lib/msg.lib.sh"

scan_usage="\ usage:   dpch scan [options]

options:
  -h, --help      show this help screen
  -Z, --debug     show debugging information
  
see 'dpch help scan' for more information"

cmd_scan() {
  trap exit 1 SIGINT
  
  [ -z "${repo}" ] \
    && repo="${PWD}"

  cd "${repo}"

  type git &>/dev/null \
    || msg__die "cannot find 'git' install"
  
  type mvn &>/dev/null \
    || msg__die "cannot find 'mvn' install"

  git rev-parse --abbrev-ref HEAD &>/dev/null \
    || msg__die "'${repo}' is not a git repository"

  supr_list="$(locate central_suppression_list/.git)"
  [ -z "${supr_list}" ] \
    msg__die "unable to locate global suppression list"

  repo_br="$(git rev-parse --abbrev-ref HEAD 2>/dev/null)"
  repo_tl="$(git rev-parse --show-toplevel 2>/dev/null)"

  repo_tl="${repo_tl##*/}"

  printf "updating nvdcve data..."

  if [ -d "${DPCH_HOME}/nvdcve" ]; then
    git -C "${DPCH_HOME}/nvdcve" pull &>/dev/null \
      && printf " done\n" \
      || printf " failed\n"
  else
    git -C "${DPCH_HOME}" clone "https://github.com/olbat/nvdcve.git" &>/dev/null \
      && printf " done\n" \
      || printf " failed\n"
  fi
      
  printf "compiling project '${repo_tl}' on branch '${repo_br}'..."

  mvn clean install -DskipTests -T 6C help:effective-pom \
    && printf " done\n" \
    || printf " failed\n"
}

main() {
  while [ -n "${1}" ]; do
    case "${1}" in
    --) shift; break ;;
    -*) case "${1}" in
    -n|--number)    [ ! -z "${2}" ] \
      && number="${2}"; shift \
      || msg__die "option '${1}' requires an argument" ;;
    -P|--proxy)     [ ! -z "${2}" ] \
      && proxy="${2}"; supplied_proxy=1; shift \
      || msg__die "option '${1}' requires an argument" ;;
    -U|--useragent) [ ! -z "${2}" ] \
      && agent="${2}"; supplied_agent=1; shift \
      || msg__die "option '${1}' requires an argument" ;;
    -n|--no-retry) no_retry=1 ;;
    -Z|--debug)    debug=1 ;;
    -h|--help)     msg__usage "${create_usage}" ;;
    -*)            msg__die "unknown option '${1}'" ;;
    esac ;;

    *) [ -z "${repo}" ] && repo="${1}" ;;
    
    esac
    shift
  
  done
  
  msg__debug "got flags '${argv%% }'"
  
  cmd_scan
}

main ${@}

exit 0
